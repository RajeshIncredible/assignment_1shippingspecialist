package main;

import java.util.List;
import java.util.Collections;
import java.util.ArrayList;

public class colorSorting {
	

	public void colorSorting(List<Widget> sampleWidgets) {
		// TODO Auto-generated method stub
		
		System.out.println("*** Before sorting:");
		 
        for (Widget wg : sampleWidgets) {
            System.out.println(wg);
        }
		
        Collections.sort(sampleWidgets, new ColorChainedSorting(
                new sortwidgetColorWise()));
                
                
        
        System.out.println("\n*** After sorting:");
        for (Widget wg : sampleWidgets) {
            System.out.println(wg);
        }
	}

}
