package main;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import main.Widget;

public class ColorChainedSorting implements Comparator<Widget> {
	 private List<Comparator<Widget>> listComparators;
	 
	    @SafeVarargs
	    public ColorChainedSorting(Comparator<Widget>... comparators) {
	        this.listComparators = Arrays.asList(comparators);
	    }
	 
	    @Override
	    public int compare(Widget w1, Widget w2) {
	        for (Comparator<Widget> comparator : listComparators) {
	            int result = comparator.compare(w1, w2);
	            if (result != 0) {
	                return result;
	            }
	        }
	        return 0;
	    }

	

	

}
